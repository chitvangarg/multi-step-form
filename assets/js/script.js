const nextButton = document.getElementById("next")
const backButton = document.getElementById("back")
const form = document.getElementById("form")
const inputElement = document.getElementsByTagName("input")
const phone_warning = document.getElementById("phone_warning")
const email_warning = document.getElementById("email_warning")
const username_warning = document.getElementById("name_warning")
const icon = document.getElementsByClassName("icon")
const price_switch = document.getElementById("toggle_switch")
const plans = document.getElementsByClassName("plan")
const plan_warning = document.getElementById("plan_warning")
const addOns = document.getElementsByClassName("add_ons")
const addonOptions = document.getElementsByClassName("options")
const stepCards = document.getElementsByClassName("step")

//  Plan change
function changeSlide() {
    let currIdx = 0
    let valid = true
    let selected = true

    const changePlan = document.getElementById("change")

    changePlan.addEventListener("click", function (e) {
        stepCards[currIdx].classList.remove("active")
        icon[currIdx].style.backgroundColor = "transparent"
        icon[currIdx].style.color = "#fff"
        currIdx = 1
        stepCards[currIdx].classList.add("active")
        icon[currIdx].style.backgroundColor = "#eef5ff"
        icon[currIdx].style.color = "#483EFF"
        nextButton.textContent = "Next Step"
    })

    nextButton.addEventListener("click", function (e) {
        e.preventDefault()

        if (currIdx === 0) {
            valid = formValidation()
        }

        if (currIdx == 1) {
            localStorage.getItem("type")
            if (localStorage.getItem("type") === null) {
                selected = false
            } else {
                selected = true
            }
        }

        if (valid === true && selected === true) {
            icon[currIdx].style.backgroundColor = "transparent"
            icon[currIdx].style.color = "#fff"
            stepCards[currIdx].classList.remove("active")

            currIdx++

            if (currIdx > 0) {
                backButton.style.visibility = "visible"
            }

            if (currIdx == stepCards.length - 2) {
                nextButton.textContent = "Confirm"
            }

            if (currIdx == stepCards.length - 1) {
                stepCards[currIdx].classList.add("regards")
                document.querySelector(".btn_group").style.display = "none"
                nextButton.textContent = "Confirm"
            }

            if (currIdx >= stepCards.length) {
                alert("no next slide")
            } else {
                icon[currIdx].style.backgroundColor = "#eef5ff"
                icon[currIdx].style.color = "#483EFF"
                stepCards[currIdx].classList.add("active")

                if (currIdx === 2) {
                    for (let addon = 0; addon < addOns.length; addon++) {
                        if (addOns[addon].checked === true) {
                            addOns[addon].checked = false
                            addOns[addon].parentNode.style.backgroundColor =
                                "transparent"
                            addOns[addon].parentNode.style.border =
                                "2px solid #ccc"
                        }
                    }
                    localStorage.removeItem("addOns")
                    createAddOnList()
                }

                if (currIdx === 3) {
                    const addons_info = document.querySelector(".add_ons_info")

                    while (addons_info.firstChild) {
                        addons_info.removeChild(addons_info.firstChild)
                    }

                    finishingUp()
                }
            }
        }

        if (selected === false) {
            plan_warning.textContent = "Please select at least one plan"
        }
    })

    backButton.addEventListener("click", function (e) {
        e.preventDefault()

        icon[currIdx].style.backgroundColor = "transparent"
        icon[currIdx].style.color = "#fff"

        stepCards[currIdx].classList.remove("active")

        if (currIdx === 3) {
            const addons_info = document.querySelector(".add_ons_info")

            while (addons_info.firstChild) {
                addons_info.removeChild(addons_info.firstChild)
            }
        }

        currIdx--

        nextButton.textContent = "Next Step"

        if (currIdx <= 0) {
            backButton.style.visibility = "hidden"
        }

        if (currIdx < stepCards.length - 1) {
            nextButton.style.display = "block"
        }

        if (currIdx < 0) {
            alert("no prev slide")
        } else {
            icon[currIdx].style.backgroundColor = "#eef5ff"
            icon[currIdx].style.color = "#483EFF"
            stepCards[currIdx].classList.add("active")
        }
    })
}

// Form Validation

function formValidation() {
    const username = document.getElementById("name")
    const email = document.getElementById("email")
    const phone = document.getElementById("phone")
    var user_reg = /^[a-z ]+$/i
    const mobileFormat = /^\+?([0-9]{2})\)?[-. ]?([0-9]{10})$/

    if (username.value === "") {
        username_warning.textContent = "Please Enter your name"
        return false
    } else if (email.value === "") {
        email_warning.textContent = "Please Enter email Address"
        return false
    } else if (phone.value === "") {
        phone_warning.textContent = "Please Enter your contact number"
        return false
    }

    if (user_reg.test(username.value) === false) {
        username_warning.textContent = "Username must contain only Alphabets"
        return false
    } else if (email.value.includes("@") === false) {
        email_warning.textContent = "Invalid Email Address"
        return false
    }else if (isNaN(phone.value) == true) {
        phone_warning.textContent = "Phone number must be numeric"
        return false
    }
    else if (mobileFormat.test(phone.value)) {
        phone_warning.textContent = "Please enter the specified format"
        return false
    }

    return true
}

for (let elem = 0; elem < inputElement.length; elem++) {
    inputElement[elem].addEventListener("focus", function () {
        username_warning.textContent = ""
        email_warning.textContent = ""
        phone_warning.textContent = ""
    })
}

// Change pricing of the plan

price_switch.addEventListener("change", function (event) {
    localStorage.clear()
    const yearly = event.target.checked
    const plan_pricing = document.getElementsByClassName("pricing")
    const free_offer = document.getElementsByClassName("free")

    const yearly_price = ["$90/yr", "$120/yr", "$150/yr"]
    const monthly_price = ["$9/mo", "$12/mo", "$15/mo"]

    for (
        let non_selected_plan = 0;
        non_selected_plan < plans.length;
        non_selected_plan++
    ) {
        plans[non_selected_plan].style.border = "2px solid #ccc"
        plans[non_selected_plan].style.backgroundColor = "#fff"
    }

    if (yearly === true) {
        document.getElementById("yearly").style.color = "black"
        document.getElementById("monthly").style.color = "#777"
        for (let plan = 0; plan < plan_pricing.length; plan++) {
            plan_pricing[plan].textContent = yearly_price[plan]
            free_offer[plan].style.visibility = "visible"
            localStorage.setItem("plan_duration", "Yearly")
        }
    } else {
        document.getElementById("monthly").style.color = "black"
        document.getElementById("yearly").style.color = "#777"
        for (let plan = 0; plan < plan_pricing.length; plan++) {
            plan_pricing[plan].textContent = monthly_price[plan]
            free_offer[plan].style.visibility = "hidden"
            localStorage.setItem("plan_duration", "Monthly")
        }
    }
})

// Select plans

for (let selected_plan = 0; selected_plan < plans.length; selected_plan++) {
    plans[selected_plan].addEventListener("click", function (e) {
        e.stopPropagation()

        plan_warning.textContent = ""

        for (
            let non_selected_plan = 0;
            non_selected_plan < plans.length;
            non_selected_plan++
        ) {
            plans[non_selected_plan].style.border = "2px solid #ccc"
            plans[non_selected_plan].style.backgroundColor = "#fff"
        }

        this.style.border = "2px solid #483eff"
        this.style.backgroundColor = "rgba(238,245,255, 0.7)"
        const plan_type = this.querySelector("h3")
        const plan_pricing = this.querySelector(".pricing")
        localStorage.setItem("type", plan_type.textContent)
        localStorage.setItem("price", plan_pricing.textContent.trim())
    })
}

// Add-ons functionality

// storing addon using closure
function addAddon() {
    let selected_addon = {}

    return function (key, value) {
        selected_addon[key] = value
        return selected_addon
    }
}

// Addon list
function createAddOnList() {
    let pickedaddon = addAddon()
    let addOnList

    const monthly_addon = ["$1/mo", "$2/mo", "$2/mo"]
    const yearly_addon = ["$10/yr", "$20/yr", "$20/yr"]

    const add_on_pricing = document.getElementsByClassName("addon_price")

    if (localStorage.getItem("plan_duration") === "Yearly") {
        for (let cost = 0; cost < yearly_addon.length; cost++) {
            add_on_pricing[cost].textContent = `+ ${yearly_addon[cost]}`
        }
    } else {
        for (let cost = 0; cost < monthly_addon.length; cost++) {
            add_on_pricing[cost].textContent = `+ ${monthly_addon[cost]}`
        }
    }

    for (let addon = 0; addon < addOns.length; addon++) {
        addOns[addon].addEventListener("change", function (e) {
            let selected = e.target.checked
            let parent = this.parentNode
            const addon_info = e.target.value
            const price = addonOptions[addon]
                .querySelector("p")
                .textContent.slice(2)

            if (selected === true) {
                e.target.backgroundColor = "#483eff"
                addOnList = pickedaddon(addon_info, price)
                parent.style.backgroundColor = "#eef5ff"
                parent.style.border = "2px solid #483eff"
            } else {
                delete addOnList[addon_info]
                parent.style.backgroundColor = "transparent"
                parent.style.border = "2px solid #ccc"
            }
            localStorage.setItem("addOns", JSON.stringify(addOnList))
        })
    }
}

// Confirmation page
function finishingUp() {
    const curr_plan = document.getElementById("curr_plan")
    const plan_pricing = document.getElementById("plan_pricing")
    curr_plan.textContent = `${localStorage.getItem(
        "type"
    )} (${localStorage.getItem("plan_duration")})`

    plan_pricing.textContent = localStorage.getItem("price")

    const addons_info = document.querySelector(".add_ons_info")

    const addons_data = JSON.parse(localStorage.getItem("addOns"))

    let total = parseInt(
        localStorage
            .getItem("price")
            .substring(1, localStorage.getItem("price").indexOf("/"))
    )

    for (let addon in addons_data) {
        const addon_container = document.createElement("div")
        addon_container.setAttribute("class", "selected_addon")

        const categ = document.createElement("div")
        const categ_pricing = document.createElement("div")
        categ.textContent = addon
        categ_pricing.textContent = `+ ${addons_data[addon]}`

        addon_container.appendChild(categ)
        addon_container.appendChild(categ_pricing)

        addons_info.appendChild(addon_container)

        console.log(
            parseInt(
                addons_data[addon].substring(1, addons_data[addon].indexOf("/"))
            )
        )
        total += parseInt(
            addons_data[addon].substring(1, addons_data[addon].indexOf("/"))
        )
    }

    const total_container = document.createElement("div")
    total_container.setAttribute("class", "total_costing")

    const total_title = document.querySelector(".total_title")
    total_title.textContent =
        localStorage.getItem("plan_duration") === "Yearly"
            ? "Total (per year)"
            : "Total (per month)"

    const total_costing = document.querySelector(".total_cost")
    total_costing.textContent =
        localStorage.getItem("plan_duration") === "Yearly"
            ? `$ ${total}/yr`
            : `$ ${total}/mo`
}

changeSlide()

// on load clear local storage

window.addEventListener("load", function (e) {
    localStorage.clear()
    localStorage.setItem("plan_duration", "Monthly")
})
